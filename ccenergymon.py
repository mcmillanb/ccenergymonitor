#!/usr/bin/env python3

import serial
import xmltodict
import time
import datetime
import urllib3
import certifi
import configparser


global config
config = configparser.ConfigParser()
config.read('config.ini')

ser = serial.Serial('/dev/ttyUSB0', 57600, timeout=1)

# Replace this with your emoncms Write API Key from the user account page
# writeApiKey = config['emoncms']['api']
writeApiKey = '0aa6709c1c6a692830a5e36eb400b734'
feedid = '389103'

# Change this if you use a different host or SSL (a must if not on a LAN)
baseAddress = 'https://emoncms.org'
unixtime = int(time.time())
# feedid = config['emoncms']['feedid']
dateString = '%Y-%m-%d %H:%M:%S'

http = urllib3.PoolManager(
    cert_reqs='CERT_REQUIRED',
    ca_certs=certifi.where())

while True:
    try:
        data = ser.readline().decode("utf-8")
        watts = int(xmltodict.parse(data)['msg']['ch1']['watts'])
        print(watts)
        tmpr = xmltodict.parse(data)['msg']['tmpr']
        if (watts >= 0 and watts <= 25000):
            inputurl = baseAddress + '/input/post?node=0&json={power1:' + str(watts) + ',temp:'+str(tmpr)+'}&apikey=' + writeApiKey
            feedurl = baseAddress + '/feed/insert.json?id='+feedid+'&time='+str(unixtime)+'&value=' + str(watts) + '&apikey=' + writeApiKey
            f = http.request('POST',feedurl)
            print(feedurl)
            i = http.request('POST',inputurl)
            print(inputurl)
    except:
        pass
time.sleep(7)